import requests 
import dropbox
import os
import sys
import csv
import pandas as pd
from pymongo import MongoClient
import json
import xlrd
import dateutil.parser
import ast
import time
import logging

logging.basicConfig(filename='/opt/isf.log', filemode='w', format='%(name)s - %(levelname)s - %(message)s')

start = time.time()
# defining the api-endpoint  
API_ENDPOINT = "http://34.210.117.170:8081/get_score_post/"
API_KEY = "TOKEN"

file_to_process='FILE'
file_object  = open("FILE_OUTPUT", "w") 


i = 0
j = 0
#try:
df = pd.read_csv(file_to_process, header=0, encoding = 'unicode_escape')

for index, row in df.iterrows():
    i = i + 1
    first_name = row["First Name"]
    last_name = row["Last Name"]
    address = row["Address"]
    zipcode = row["Zip Code"] 
    #first_name = "MARK"
    #last_name = "SPLITTGERBER"
    #address = "5705 ASPEN DR"
    #zipcode = "50266"
    file_object.write("Processing: " + str(first_name) + " " + str(last_name) + " " + str(address) + " " + str(zipcode) + "\n")
    logging.warning("Processing: " + str(first_name) + " " + str(last_name) + " " + str(address) + " " + str(zipcode))
    data = {
            'first_name':first_name, 
            'last_name':last_name, 
            'address':address,
            'zipcode':zipcode
            }     
    head = {'x-access-token': API_KEY}
    # sending post request and saving response as response object 
    r = requests.post(url = API_ENDPOINT,headers=head,data = data) 
    
    pastebin_url = r.text
    if "unknown" not in pastebin_url or "skipped" not in pastebin_url:
        j = j + 1
        rate =  (j/i) * 100
        logging.warning("Processed: " + str(i))
        print("Processed: " + str(i))
        logging.warning("Total Matched: " + str(j) + " Rate: " + str(rate) + " %")
        print(data)
        logging.warning(data)
        print(pastebin_url)
        k = json.loads(pastebin_url)
        logging.warning(pastebin_url)
        file_object.write("Processed: " + str(i) + "\n")
        file_object.write("Total Matched: " + str(j) + " Rate: " + str(rate) + " % \n") 
        file_object.write(str(data))
        file_object.write(str(pastebin_url)) 
        file_object.write("\n")
        if k["success"] == "true":
                print("Successfull" + k["message"])
                df.loc[index,"Score1"] = k["message"]
        else:
                df.loc[index,"Score1"] = "unknown"
                print("Nothing Found")
    else:
        df.loc[index,"Score1"] = "unknown"



df.to_csv( "/opt/test_processed.csv",index=False, sep=',')
end = time.time()
print("Processed records: " + str(i))
print("Processing Time: " + str(end - start))
file_object.write("Processing Time: " + str(end - start))
file_object.close() 