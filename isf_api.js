
const express       = require('express')
var app = express();
const detectIp      = require('ipware')().get_ip
const querystring = require('querystring');
const http = require('http');
var bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: "false" }));
app.use(bodyParser.json());
const request = require('request')

async function callapi(first_name,last_name,address,zipcode){ 
   return new Promise( (resolve, reject) => {

    try{
        request.post('http://34.210.117.170:8081/get_score_post/', {
    headers: {
      'x-access-token':'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjViMmQ1NzIyYmQ2ZDg1MjEwMWJjYmQ3ZiIsImlhdCI6MTU1ODQ2NTkzMiwiZXhwIjoxNTU4NTUyMzMyfQ'
    },
    json: {
      first_name: first_name,
      last_name : last_name,
      address : address,
      zipcode : zipcode,
    }
  }, (error, res, body) => {
    if (error) {
      return
    }

//success je true success
// ak false tak hned unknown
  var score = "unknown"
  if(body['success'] === "true" && body['message'] !== "unknown"){
    score = body['message'].toString()
  }
  resolve(score)
  })
    }catch(error){
        reject("unknown")
        }
      });
}



app.post('/post_leadspedia', async function (req, res) {

  var first_name = (req.body.first_name || '')
  var last_name = (req.body.last_name || '')
  var phone_cell = (req.body.phone_cell || '')
  phone_cell = phone_cell.replace("(", "");
  phone_cell = phone_cell.replace("-", "");
  phone_cell = phone_cell.replace(")", "");
  phone_cell = phone_cell.replace(" ", "");
  var phone_home = (req.body.phone_home || '')
  phone_home = phone_home.replace("(", "");
  phone_home = phone_home.replace("-", "");
  phone_home = phone_home.replace(")", "");
  phone_home = phone_home.replace(" ", "");
  var phone_work = (req.body.phone_work || '')
  phone_work = phone_work.replace("(", "");
  phone_work = phone_work.replace("-", "");
  phone_work = phone_work.replace(")", "");
  phone_work = phone_work.replace(" ", "");
  var phone_ext = (req.body.phone_ext || '')
  phone_ext = phone_ext.replace("(", "");
  phone_ext = phone_ext.replace("-", "");
  phone_ext = phone_ext.replace(")", "");
  phone_ext = phone_ext.replace(" ", "");
  var address = (req.body.address || '')
  var address2 = (req.body.address2 || '')
  var state = (req.body.state || '')
  var city = (req.body.city || '')
  var zip_code = (req.body.zip_code || '')
  var country = (req.body.country || '')
  var email_address = (req.body.email_address || '')
  var dob = (req.body.dob || '');
  var ip_address = (req.connection.remoteAddress || '')
  var ssn = (req.body.ssn || '')
  var loan_amount = (req.body.loan_amount || '')
  var loan_purpose = (req.body.loan_purpose || '')
  var gross_monthly_income = (req.body.gross_monthly_income || '')
  var income_type = (req.body.income_type || '')
  var payroll_type = (req.body.payroll_type || '')
  var payroll_frequency = (req.body.payroll_frequency || '')
  var last_payroll_date = (req.body.last_payroll_date || '')
  var next_payroll_date = (req.body.next_payroll_date || '')
  var bank_name = (req.body.bank_name || '')
  var aba_routing_number = (req.body.aba_routing_number || '')
  var account_number = (req.body.account_number || '')
  var Account_type = (req.body.Account_type || '')
  var account_length = (req.body.account_length || '')
  var employer_name = (req.body.employer_name || '')
  var hire_date = (req.body.hire_date || '')
  var leadIdentifier = (req.body.leadIdentifier || '')
  var agentId = (req.body.agentId || '')
  var status = (req.body.status || '')
  var aff_id = (req.body.aff_id || '')
  var subid = aff_id
  var subid2 = (req.body.subid || '')
  var subid3 = (req.body.subid2 || '')
  var subid4 = (req.body.subid3 || '')
  var subid5 = (req.body.subid4 || '')
  var offer_id = (req.body.offer_id || '')
  var lp_offer_id = '208'
  var lp_campaign_id = '5d149b986ce78'
  
  score = await callapi(first_name,last_name,address,zip_code)

  var data2 = {
    'first_name': first_name,
    'last_name' : last_name,
    'phone_cell' : phone_cell,
    'phone_home' : phone_home,
    'phone_work' : phone_work,
    'phone_ext' : phone_ext,
    'address':address,
    'address2':address2,
    'zip_code':zip_code,
    'email_address':email_address,
    'dob':dob,
    'ip_address':ip_address,
    'ssn':ssn,
    'loan_amount':loan_amount,
    'loan_purpose':loan_purpose,
    'gross_monthly_income':gross_monthly_income, 
    'income_type':income_type,
    'payroll_type':payroll_type,
    'payroll_frequency':payroll_frequency,
    'last_payroll_date':last_payroll_date,
    'next_payroll_date':next_payroll_date,
    'bank_name':bank_name,
    'aba_routing_number':aba_routing_number,
    'account_number':account_number,
    'Account_type':Account_type,
    'account_length':account_length,
    'employer_name':employer_name,
    'hire_date':hire_date,
    'leadIdentifier':leadIdentifier,
    'agentId':agentId,
    'status':status,
    'score':score,
    'aff_id':aff_id,
    'subid':subid,
    'subid2':subid2,
    'subid3':subid3,
    'subid4':subid4,
    'subid5':subid5,
    'offer_id':offer_id,
    'lp_offer_id':lp_offer_id,
    'lp_campaign_id':lp_campaign_id,
  }

  try{
      var options = { method: 'POST',
      url: 'https://smartmarket.leadspediatrack.com/post.do',
      headers: 
      { 'Content-Type': 'application/x-www-form-urlencoded' },
      form: data2 };
      request(options, function (error, response, body) {
        if (error) throw new Error(error);
        res.end(score);
      });
  }catch(error ){
    console.log(error)
  }



});

app.use(function (req, res) {
    error_message = '{"success":"false", "message":"Missing parameter"}'
    res.status(404).send(error_message)
})

var server = app.listen(8081, function () {
   var host = server.address().address
   var port = server.address().port
   console.log("Example app listening at http://%s:%s", host, port)
})


